# Docker  PFC 

## Actualizar Imagen
```shell
docker pull registry.gitlab.com/public-unrn/concurrente
```



## Version para Windows (WINE)

### Docker RUN  PowerShell (windows)
```shell
docker run -it --rm -v ${PWD}:/project -w/project registry.gitlab.com/public-unrn/concurrente wine cmd
```
### Docker RUN (wine CMD) terminal Ubuntu
```shell
docker run --rm -it -v $(pwd):/project -w/project  registry.gitlab.com/public-unrn/concurrente wine cmd
```


#### Ejecutar pfc
```shell
$: cd ..
$: cd pfc
$: pfc <nombreFilePas>
```


## Version para LINUX /    (DOS Debian)
### Docker RUN linux / mac
```shell
docker run --rm -it -v $(pwd):/project -w/project  registry.gitlab.com/public-unrn/concurrente:1.0
```

#### Ejecutar pfc
```shell
$: pfc <nombreFilePas>
```



